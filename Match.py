#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Match.py
#  
#  Copyright 2015 mint <mint@mint-VirtualBox>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

class Match(object):
	
	def __init__(self, playerA, playerB):
		self.playerA = playerA
		self.playerB = playerB
		
	def doMatch(self, resultA, resultB):	
		self.resultA = resultA
		self.resultB = resultB
		
		probWinA = float(1)/(1 + 10**((self.playerB.season_rating - self.playerA.season_rating)/400))
		probWinB = float(1)/(1 + 10**((self.playerA.season_rating - self.playerB.season_rating)/400))
		
		if resultA > resultB:
			print "{0} ha vencido a {1} en un enfrentamiento por {2}-{3}".format(self.playerA.name, self.playerB.name, resultA, resultB)
			self.playerA.season_rating = self.calculateRating(self.playerA, 1, probWinA)
			self.playerB.season_rating = self.calculateRating(self.playerB, 0, probWinB)

		elif resultB > resultA:
			print "{0} ha perdido contra {1} en un enfrentamiento por {2}-{3}".format(self.playerA.name, self.playerB.name, resultA, resultB)
			self.playerA.season_rating = self.calculateRating(self.playerA, 0, probWinA)
			self.playerB.season_rating = self.calculateRating(self.playerB, 1, probWinB)

		else:
			print "{0} y {1} han empatado con una puntuación de {2}-{3}".format(self.playerA.name, self.playerB.name, resultA, resultB)
			self.playerA.season_rating = self.calculateRating(self.playerA, 0.5, probWinA)
			self.playerB.season_rating = self.calculateRating(self.playerB, 0.5, probWinB)
	
	def getWinningChances(self):
		probWin = float(1)/(1 + 10**((self.playerB.season_rating - self.playerA.season_rating)/500))
		print "{0} tiene un {1:.0f}% de probabilidades de ganar en un enfrentamiento contra {2}".format(self.playerA.name, (probWin * 100), self.playerB.name)
		
		return probWin

	def calculateRating(self, player, result, expected):
		if result == 0.5:
			new = round(player.season_rating + (player.k * (float(result) - expected)))*2
		else:
			new = round(player.season_rating + (player.k * (float(result) - expected)))
		diff = round(new - player.season_rating)
		
		print "{0} [{1}] ({2})".format(player.name, new, diff)
		return new

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import time

class Contestant(object):

	def __init__(self, name, alias):
		self.name = name
		self.alias = alias
		self.dateJoined = time.strftime("%d/%m/%Y")
		self.rating = 1000
		self.k = 30
		self.victories = 0
		self.defeats = 0
		self.draws = 0
	
	def __repr__(self):
		text = "{0}\nSe unió a la liga el {1}.".format(self.name, self.dateJoined)
		text = text + "\nPuntos {0} | Rango {1}".format(self.rating, self.k)
		text = text + "\nVictorias {0} | Derrotas {1} | Empates {2}".format(self.victories, self.defeats, self.draws)
		text = text + "\nMedia de Rondas {2}".format(self.victories - defeats)		
		return text
		
	def rankUp(self):
		if self.k == 10:
			print "{0} está en su nivel máximo".format(self.name)
		else:
			self.k = self.k - 1
			print "{0} ha subido de rango".format(self.name)
	
	def rankDown(self):
		if self.k == 20:
			print "{0} está en su nivel mínimo".format(self.name)
		else:
			self.k = self.k + 1
			print "{0} ha bajado de rango".format(self.name)
	
	def confirm(self):
		if self.k == 30:
			self.k = 20
			print "{0} ha salido de la fase de pruebas".format(self.name)
		else:
			print "¡{0} ya está confirmado!".format(self.name)

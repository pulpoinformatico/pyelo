#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from Season import *
from Match import *
from Contestant import *
import db

def main():
	
	elder = Contestant("Daniel Velasco", "Elder")
	print elder
	
	database = db()
	
	database.insertPlayer(elder)

if __name__ == '__main__':
	main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import time
from peewee import *

class Contestant(BaseModel):

	id = PrimaryKeyField()
	name = CharField()
	alias = CharField()
	date_joined = DateField()
	global_rating = FloatField()
	k_factor = IntegerField()
	victories = IntegerField()
	defeats = IntegerField()
	draws = IntegerField()
	
	def __init__(self, name, alias, date_joined):
		self.name = name
		self.alias = alias
		self.date_joined = date_joined
		self.global_rating = 1000
		self.k_factor = 30
		self.victories = 0
		self.defeats = 0
		self.draws = 0
	
	def __repr__(self):
		text = "{0} \"{1}\"\nSe unió a la liga el {1}.".format(self.name, self.alias, self.dateJoined)
		text = text + "\nPuntos {0} | Rango {1}".format(self.rating, self.k)
		text = text + "\nVictorias {0} | Derrotas {1} | Empates {2}".format(self.victories, self.defeats, self.draws)
		text = text + "\nMedia de Rondas {0}".format(self.victories - self.defeats)
		return text
	
	def rankUp(self):
		if self.k == 10:
			print "{0} está en su nivel máximo".format(self.name)
		else:
			self.k = self.k - 1
			print "{0} ha subido de rango".format(self.name)
	
	def rankDown(self):
		if self.k == 20:
			print "{0} está en su nivel mínimo".format(self.name)
		else:
			self.k = self.k + 1
			print "{0} ha bajado de rango".format(self.name)
	
	def confirm(self):
		if self.k == 30:
			self.k = 20
			print "{0} ha salido de la fase de pruebas".format(self.name)
		else:
			print "¡{0} ya está confirmado!".format(self.name)

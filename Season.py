#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Season.py
#  
#  Copyright 2015 mint <mint@mint-VirtualBox>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from Contestant import *

class Season(object):
	
	number = 1
	
	def __init__(self, start):
		self.number = Season.number
		self.start_date = start
		self.ended = False
		self.players = []
		self.days = []
		
		Season.number += 1
	
	class SeasonDay(object):
	
		def __init__(self, day_date):
			self.date = day_date
			self.matches = []
		
		def addMatch(self, match):
			self.matches.append(match)
			
	def addDay(self, date):
		self.days.append(Season.SeasonDay(date))
	
	def printDay(self):
		for day in self.days:
			print "{0}".format(day.date)
			for match in day.matches:
				if match.resultA is None:
					print "  {0} - {1}".format(match.playerA, match.playerB)
				else:
					print "  {0} {1} - {2} {3}".format(match.playerA, match.resultA, match.playerB, match.resultB)

	class Player(object):
	
		PlayerID = 1
		
		def __init__(self, participant):
			self.playerID = Season.Player.PlayerID
			self.name = participant.name
			self.season_rating = 1000
			self.global_rating = participant.rating
			self.k = participant.k
			self.in_season = True	
			
	def addPlayer(self, participant):
		self.players.append(Season.Player(participant))
	
	def delPlayer(self, name):
		for pl in self.players:
			if pl.name == name:
				self.players.remove(pl)

	def printRanking(self):
		print "Temporada {0}".format(self.number)
		for player in self.players:
			print "    {0} - {1} ptos.".format(player.name, player.season_rating)
			
	def printSeason(self):
		for day in self.days:
			day.printDay()
	
	def endSeason():
		self.ended = True
